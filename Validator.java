
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**Validator class
 * Contains main class.
 * Specifies the validation method
 * @author Anna Derkach
 * @version 1.0
 */

public class Validator {

	public static void main(String[] args) {
		while (true) {
			System.out
					.println("Available commands: \n1-validation by regular expression  \n2-stack validation \n3-exit");
			Validation v = null;
			Scanner s = new Scanner(System.in);
			String str = s.nextLine();
			switch (str) {
			case "1":
				v = new RegularExpressionValidation();
				break;
			case "2":
				v = new StackValidation();
				break;
			case "exit":
				System.exit(0);
			}
			String str1 = s.nextLine();
			System.out.println(v.validate(str1));
		}
	}

}

/**Interface
 * Contains only one method for implementation.
 */
interface Validation {
	public boolean validate(String htmlCode);
}

/**RegularExpressionValidation class
 * Validates html string using regular expression.
 * Implements Validation interface
 */
class RegularExpressionValidation implements Validation {
	private static final String s = "<(\\w+)(?=[\\s>])[^>\\w]*>((?:(?!</\\1>).)*)</\\1>";
	
	/**
	 * Implemented method.
	 * Returns true if the string is correct.
	 */
	public boolean validate(String htmlCode) {
		Pattern pattern;
		Matcher matcher;
		Pattern p = Pattern.compile(s);
		Matcher m = p.matcher(htmlCode);
		return m.matches();
	}
}

/**StackValidation class
 * Validates html string using stack.
 * Implements Validation interface.
 */
class StackValidation implements Validation {
	private Stack<Character> tagstack;

	/**
	 * Implemented method.
	 * Returns true if the string is correct.
	 */
	public boolean validate(String htmlCode) {
		tagstack = new Stack<Character>();
		Map<Integer, String> map = new HashMap<Integer, String>();
		boolean isBalanced = false;
		int flag = 0, index = 0, isSlesh = 0;
		for (int i = 0; i < htmlCode.length(); i++) {
			if (htmlCode.charAt(i) == '<') {
				flag = i;
				tagstack.push('<');
			}
			if (htmlCode.charAt(i) == '/')
				isSlesh = i;

			if (htmlCode.charAt(i) == '>') {
				if (tagstack.isEmpty())
					return isBalanced;
				if (isSlesh == 0) {
					index++;
					map.put(index, htmlCode.substring(flag + 1, i));
				} else {
					if (!map.isEmpty())
						if (htmlCode.substring(isSlesh + 1, i).equals(
								map.get(index))) {
							map.remove(index);
							index--;
							isSlesh = 0;
						} else
							return isBalanced;
				}
				tagstack.pop();
			}
		}
		if (tagstack.isEmpty())
			isBalanced = true;
		return isBalanced;
	}
}